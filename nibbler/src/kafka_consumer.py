"""
Provides Consumer class which connects to Kafka queue and consumes
messages from desired topic.
"""
import json
import logging

from kafka import KafkaConsumer

log = logging.getLogger(__name__)

DEFAULT_POLL_TIMEOUT_MS = 1000
DEFAULT_POLL_MAX_RECORDS = 50


class Consumer:
    """Provides an ability to consume messages from Kafka message queue."""

    def __init__(self, host, port, topic, cert_dir):
        self.consumer = KafkaConsumer(
            topic,
            bootstrap_servers=f"{host}:{port}",
            auto_offset_reset="latest",
            security_protocol="SSL",
            ssl_cafile=f"{cert_dir}/ca.pem",
            ssl_certfile=f"{cert_dir}/service.cert",
            ssl_keyfile=f"{cert_dir}/service.key",
            value_deserializer=lambda m: json.loads(m.decode("utf-8")),
            key_deserializer=lambda m: json.loads(m.decode("utf-8"))
        )

    def get_messages(self,
                     timeout_ms=DEFAULT_POLL_TIMEOUT_MS,
                     max_records=DEFAULT_POLL_MAX_RECORDS):
        """Polls Kafka topic for messages."""

        data = {}
        try:
            data = self.consumer.poll(timeout_ms=timeout_ms,
                                      max_records=max_records,
                                      update_offsets=True)
        except json.JSONDecodeError as error:
            log.error(error)

        return data.values()
