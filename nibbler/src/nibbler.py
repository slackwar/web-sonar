#!/usr/bin/env python
"""
Provides Nibbler class and CLI interface to the nibbler tool.
Nibbler feasts on messages from Kafka topic and digests them into database
records.
"""
import logging
import signal

import click

import pg_writer
import kafka_consumer


logging.basicConfig(level=logging.INFO)
log = logging.getLogger(__name__)


class SignalHandler:
    """Allows a graceful handling of SIGINT and SIGTERM signals."""

    quit = False

    def __init__(self):
        signal.signal(signal.SIGINT, self.exit_now)
        signal.signal(signal.SIGTERM, self.exit_now)

    def exit_now(self, *_):  # signum, frame - are not used
        """Set quit flag to True."""
        self.quit = True
        log.debug("Received termination signal")

    def get_status(self):
        """Return quit flag."""
        return self.quit


class Nibbler:
    """
    Consumes messages from Kafka queue, creates new records for the freshly
    registered URLs and populates report table with site request stats.
    """
    def __init__(self, consumer, db_writer):
        self.consumer = consumer
        self.db_writer = db_writer
        self.sighandler = SignalHandler()

    def digest(self, message):
        """Process a single message and update the correspondent table."""

        if "register" in message.value:
            url = message.value["url"]
            has_regex = "regex" in message.value
            regex = message.value["regex"] if has_regex else None

            log.debug("Registering new URL: %s", url)
            self.db_writer.register(url, regex)
        else:
            self.db_writer.report(message)

    def feast(self):
        """
        Continiously consume messages from Kafka queue and digest them.
        """
        while not self.sighandler.get_status():
            for messages in self.consumer.get_messages():
                for message in messages:
                    self.digest(message)


@click.command()
@click.option("--service_uri",
              help="PostgreSQL database connection string", required=True)
@click.option("--kafka_host", help="Kafka host", required=True)
@click.option("--kafka_port", help="Kafka port", required=True)
@click.option("--kafka_topic", default="sonar", help="Kafka topic")
@click.option("--cert_dir",
              help="Full path to the directory with SSL certs.", required=True)
def main(service_uri, kafka_host, kafka_port, kafka_topic, cert_dir):
    """
    Consumes messages from Kafka topic sent by Sonar and populates PostgreSQL
    database with ping results.
    """

    consumer = kafka_consumer.Consumer(
        kafka_host,
        kafka_port,
        kafka_topic,
        cert_dir)
    db_writer = pg_writer.DBWriter(service_uri)

    nibbler = Nibbler(consumer, db_writer)
    nibbler.feast()
