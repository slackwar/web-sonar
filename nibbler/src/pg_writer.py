"""
Provides DBWriter class, which encapsulates the following database operations:
 - Table creation on init if required
 - New URL registration
 - Storage of site ping reports
"""
import logging
import psycopg2

log = logging.getLogger(__name__)


class DBWriter:
    """Encapsulates all PostgreSQL database opeartions"""
    def __init__(self, uri):
        self.service_uri = uri
        self.db_conn = None
        self.register_id = None

        create_register = """
            CREATE TABLE IF NOT EXISTS register (
                register_id serial PRIMARY KEY,
                url         TEXT   UNIQUE NOT NULL,
                regex       TEXT
            );
            """

        create_report = """
            CREATE TABLE IF NOT EXISTS report (
                register_id  INT          NOT NULL,
                time_stamp   TIMESTAMPTZ  NOT NULL,
                request_time NUMERIC      NOT NULL,
                result_code  NUMERIC(3,0) NOT NULL,
                regex_ok     BOOL,
                PRIMARY KEY (register_id, time_stamp),
                FOREIGN KEY (register_id)
                    REFERENCES register (register_id)
            );
            """
        cursor = None
        log.info("Creating tables")
        try:
            self.db_conn = psycopg2.connect(self.service_uri)
            cursor = self.db_conn.cursor()
            cursor.execute(create_register)
            cursor.execute(create_report)
        except psycopg2.Error as error:
            log.error(error)
        finally:
            if cursor is not None:
                cursor.close()

    def find_register_id(self, url):
        """Returns correspondent register_id for URL"""
        find_query = """SELECT register_id FROM register WHERE url == %s;"""
        register_id = None
        cursor = None
        log.info("Find register_id for URL: %s", url)
        try:
            cursor = self.db_conn.cursor()
            cursor.execute(find_query, (url))
            register_id = cursor.fetchone()[0]
        except psycopg2.Error as error:
            log.error(error)
        finally:
            if cursor is not None:
                cursor.close()

        return register_id

    def register(self, url, regex):
        """Processes a new URL registration."""
        register_query = """
            INSERT INTO register (url, regex)
            VALUES(%s, %s)
            ON CONFLICT (url) DO UPDATE SET regex = EXCLUDED.regex
            RETURNING register_id;
            """
        cursor = None
        log.info("Register URL: %s", url)
        try:
            cursor = self.db_conn.cursor()
            cursor.execute(register_query, (url, regex))
            self.register_id = cursor.fetchone()[0]
        except psycopg2.Error as error:
            log.error(error)
        finally:
            if cursor is not None:
                cursor.close()

    def report(self, message):
        """Adds an entry into report table."""

        url = message.value['url']
        if self.register_id is None:
            # Assuming that the URL was registered
            self.register_id = self.find_register_id(url)

        request_time = message.value['time']
        result_code = message.value['result_code']
        regex_ok = message.value.get('regex_ok')
        report_query = """
            INSERT INTO report (
                register_id,
                time_stamp,
                request_time,
                result_code,
                regex_ok)
            VALUES(%s, to_timestamp(%s), %s, %s, %s);
            """
        cursor = None
        log.info("Report stats for URL: %s", url)
        try:
            cursor = self.db_conn.cursor()
            cursor.execute(report_query, (
                self.register_id,
                message.timestamp,
                request_time,
                result_code,
                regex_ok
            ))
        except psycopg2.Error as error:
            log.error(error)
        finally:
            if cursor is not None:
                cursor.close()

    def __del__(self):
        if self.db_conn is not None:
            self.db_conn.close()
            log.debug("Database connection closed.")
