# Web-sonar

Project contains two tools which work together organising a pipeline for web site availability statistics.

**Sonar** - simple web site ping app with Kafka integration.

```
sonar --url="https://example.com" --regex="domain" --interval=<in seconds> \
    --kafka_host=<kafka-host> \
    --kafka_port=<kafka-port> \
    --kafka_topic=<kafka-topic> \
    --cert_dir="/full/path/to/kafka/certs/"
```

**Nibbler** - tool is a Kafka consumer which puts ping results into a PostgreSQL database.

```
nibbler --service_uri=postgres://<pguser>:<pgpassword@<pg-host>:<pg-port/<database> \
    --kafka_host=<kafka-host> \
    --kafka_port=<kafka-port> \
    --kafka_topic=<kafka-topic> \
    --cert_dir="/full/path/to/kafka/certs"
```
