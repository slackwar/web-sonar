import io
from setuptools import find_packages, setup

PACKAGE_NAME = 'sonar'
PACKAGE_VERSION = '0.1.0'

requires = [
    'click>=7.0.0,<8',
    'requests>=2.24.0',
    'kafka-python>=2.0.2',
]

test_requires = [
    'codecov',
    'flake8',
    'pytest',
    'pytest-cov',
    'pytest-helpers-namespace',
    'pytest-mock',
    'pytest-faker',
]

with io.open('README.md', mode='r', encoding='utf-8') as f:
    readme = f.read()

setup(
    author='Alexander Fomichev',
    name=PACKAGE_NAME,
    version=PACKAGE_VERSION,
    description='Simple web site availability checker',
    url='https://gitlab.com/slackwar/web-sonar/sonar',
    long_description=readme,
    long_description_content_type="text/markdown",
    setup_requires=["pytest-runner"],
    install_requires=requires,
    tests_require=test_requires,
    license='MIT',
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Developers',
        'Intended Audience :: Information Technology',
        'Topic :: System :: Systems Administration',
        'Topic :: Utilities',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
    ],
    python_requires='>=3.6,<4',
    packages=find_packages('src'),
    package_dir={'': 'src'},
    package_data={
        'web-sonar': [''],
    },
    include_package_data=True,
    entry_points={
        'console_scripts': [
            'sonar=sonar:main',
        ],
    },
    extras_require={'test': test_requires},
)
