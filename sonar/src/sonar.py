#!/usr/bin/env python
"""
Provides Sonar class and CLI interface to the sonar tool.
Sonar pings web sites and produces reports into Kafka topic.
"""
import logging
import signal
import sys
import time

import click

import ping
import kafka_producer


logging.basicConfig(level=logging.INFO)
log = logging.getLogger(__name__)


# example was found here:
# https://stackoverflow.com/questions/18499497/how-to-process-sigterm-signal-gracefully
class SignalHandler:
    """Allows a graceful handling of SIGINT and SIGTERM signals."""
    quit = False

    def __init__(self):
        signal.signal(signal.SIGINT, self.exit_now)
        signal.signal(signal.SIGTERM, self.exit_now)

    def exit_now(self, *_):  # signum, frame - are not used
        """Set quit flag to True."""
        self.quit = True

    def get_status(self):
        """Return quit flag."""
        return self.quit


class Sonar:
    """
    Contains main sonar pulse functionality.
    """
    def __init__(self, site_url, interval, regex, producer):
        self.url = site_url
        self.interval = interval
        self.regex = regex
        self.producer = producer
        self.sighandler = SignalHandler()

        registration_data = {
            "url": self.url,
            "register": True,
        }
        if self.regex:
            registration_data["regex"] = self.regex
        log.info("Register URL: %s", self.url)
        producer.report(registration_data)

    def wait_for(self, seconds):
        """
        To increase responsiveness on signal interruption, method checks
        for the signal status every second during sleep.
        """
        seconds_left = seconds
        while not self.sighandler.get_status() and seconds_left > 0:
            time.sleep(1)
            seconds_left -= 1
        if self.sighandler.get_status():
            sys.exit(0)

    def pulse(self):
        """Start sendig requests to the target url with given interval."""
        while True:
            record = ping.ping(
                self.url,
                ping.get_content if self.regex else ping.get_head,
                self.regex
            )
            log.info("Report ping stats for URL: %s", self.url)
            self.producer.report(record)
            self.producer.flush()
            self.wait_for(self.interval)


@click.command()
@click.option("--url", help="Target URL to ping", required=True)
@click.option("--interval", default=10, help="Target ping interval in seconds")
@click.option("--regex", default=None, help="Optional search pattern")
@click.option("--kafka_host", help="Kafka host", required=True)
@click.option("--kafka_port", help="Kafka port", required=True)
@click.option("--kafka_topic", default="sonar", help="Kafka topic")
@click.option("--cert_dir",
              help="Full path to the directory with SSL certs.", required=True)
def main(url, interval, regex, kafka_host, kafka_port, kafka_topic, cert_dir):
    """
    Pings the given url with the given interval and optionally searches for
    a regex in the received content.
    """
    producer = kafka_producer.Producer(
        kafka_host,
        kafka_port,
        kafka_topic,
        cert_dir)

    sonar = Sonar(url, interval, regex, producer)
    sonar.pulse()
