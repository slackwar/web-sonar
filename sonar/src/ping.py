"""
Provides Sonar ping functionality.
Ping is a simple HEAD or GET request if regex was provided.
"""
import logging
import re
import time
import requests


ERR_CONNECTION = 111  # Failed to connect
ERR_UNKNOWN = 112  # Unknown failure, no errno provided

HTTP_OK = 200
HTTP_MULTIPLE_CHOICES = 300

log = logging.getLogger(__name__)


def get_head(site_url):
    """Send HEAD request and return headers"""
    response = requests.head(site_url, allow_redirects=True)

    return response


def get_content(site_url):
    """Send GET request and return the content"""
    response = requests.get(site_url)

    return response


def is_status_ok(status):
    """Check if the given status is OK"""
    return HTTP_OK <= status < HTTP_MULTIPLE_CHOICES


def ping(site_url, signaller=get_head, regex=None):
    """
    Send a request to the given URL and return results
    """
    result = {"url": site_url}
    try:
        start_time = time.time()
        data = signaller(site_url)
        total_time = time.time() - start_time

        result["time"] = total_time
        result["result_code"] = data.status_code

        # searching for the regex only if we successfully got a page
        if regex and is_status_ok(data.status_code):
            found = re.search(regex, data.content.decode())
            if found:
                result["regex_ok"] = True
            else:
                result["regex_ok"] = False

    except (requests.ConnectionError, ConnectionError) as error:
        log.error(error)
        result = {
            "url": site_url,
            "time": 0,
            "result_code": ERR_CONNECTION,
        }
    except requests.RequestException as error:
        log.error(error)
        result = {
            "url": site_url,
            "time": 0,
            "result_code": ERR_UNKNOWN,
        }

    return result
