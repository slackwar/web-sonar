"""
Provides Producer class which puts messages into desired Kafka topic.
"""
import json

from kafka import KafkaProducer


# example from:
# https://github.com/aiven/kafka-python-fake-data-producer/blob/main/main.py
class Producer:
    """Provides an ability to produce messages into Kafka message queue."""
    def __init__(self, host, port, topic_name, cert_dir):
        self.topic = topic_name
        self.producer = KafkaProducer(
            bootstrap_servers=f"{host}:{port}",
            security_protocol="SSL",
            ssl_cafile=f"{cert_dir}/ca.pem",
            ssl_certfile=f"{cert_dir}/service.cert",
            ssl_keyfile=f"{cert_dir}/service.key",
            value_serializer=lambda v: json.dumps(v).encode("utf-8"),
            key_serializer=lambda v: json.dumps(v).encode("utf-8")
        )

    def report(self, data):
        """Send data into Kafka topic."""
        key = {"url": data["url"]}
        self.producer.send(self.topic, key=key, value=data)

    def flush(self):
        """Flush buffers into queue."""
        self.producer.flush()
